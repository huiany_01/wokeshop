package io.datajek.spring.basics.movierecommendersystem.lesson12;


import javax.inject.Named;
import org.springframework.beans.factory.annotation.Qualifier;

@Named
@Qualifier("CF")
public class CollaborativeFilter implements Filter{

    public String[] getRecommendations(String movie) {
        //协作式过滤器的逻辑
        return new String[] {"Finding Nemo", "Ice Age", "Toy Story"};
    }
}