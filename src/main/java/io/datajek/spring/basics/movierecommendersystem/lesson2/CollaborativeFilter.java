package io.datajek.spring.basics.movierecommendersystem.lesson2;

public class CollaborativeFilter implements Filter{

    public String[] getRecommendations(String movie) {
        //基于协作过滤器的逻辑
        return new String[] {"Finding Nemo", "Ice Age", "Toy Story"};
    }
}