package io.datajek.spring.basics.movierecommendersystem.lesson3;

import org.springframework.stereotype.Component;

@Component
public class RecommenderImplementation {

    //@Autowired
    private Filter filter;

    public RecommenderImplementation(Filter filter){
        super();
        this.filter=filter;
    }

    public String[] recommendMovies (String movie){

        System.out.println("正在使用的过滤器名称:"+filter+"\n");

        String[] results=filter.getRecommendations("Finding Dory");

        //返回结果
        return results;
    }
}
