package io.datajek.spring.basics.movierecommendersystem.lesson6;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class RecommenderImplementation {

    @Autowired
    @Qualifier("CF")
    private Filter contentBasedFilter;


    public String[] recommendMovies (String movie){

        System.out.println("正在使用的过滤器名称:"+contentBasedFilter+"\n");

        String[] results=contentBasedFilter.getRecommendations("Finding Dory");

        //返回结果
        return results;
    }
}
