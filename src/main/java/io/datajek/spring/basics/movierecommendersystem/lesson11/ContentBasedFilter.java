package io.datajek.spring.basics.movierecommendersystem.lesson11;

import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;


@Component
public class ContentBasedFilter implements Filter {

    private Logger logger= LoggerFactory.getLogger(this.getClass());

    public ContentBasedFilter(){
        super();
        logger.info("In ContentBasedFilter constructor method");
    }

    @PostConstruct
    private void postConstruct(){
        logger.info("In ContentBasedFilter postConstruct method");
    }

   @PreDestroy
    private void preDestroy() {
        //从缓存中清除电影
        logger.info("In ContentBasedFilter preDestroy method");
    }

    public String[] getRecommendations(String movie){
        //实现基于内容的过滤器的逻辑
        return new String[] {
          "Happy Feet","Ice Age","Shark Tale"
        };
    }
}
