package io.datajek.spring.basics.movierecommendersystem.lesson11;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class RecommenderImplementation {
    
    private Logger logger= LoggerFactory.getLogger(this.getClass());

    private Filter filter;

    @Autowired
    public void setFilter(Filter filter){
        logger.info("In RecommenderImplementation setter method..dependency injection");
        this.filter=filter;
    }

    public String [] recommendMovies (String movie){

        String[] results=filter.getRecommendations("Finding Dory");

        //返回结果
        return results;
    }

    @PostConstruct
    public void postConstruct(){
        //初始化代码
        logger.info("In RecommenderImplementation postConstruct method");
    }

    @PreDestroy
    public void preDestroy() {
        //用于清理的代码
        logger.info("In RecommenderImplementation preDestroy method");
    }
}
