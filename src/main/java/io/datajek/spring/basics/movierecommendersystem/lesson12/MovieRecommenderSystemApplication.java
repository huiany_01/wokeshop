package io.datajek.spring.basics.movierecommendersystem.lesson12;

import java.util.Arrays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class MovieRecommenderSystemApplication {

	public static void main(String[] args) {

		//ApplicationContext负责管理Bean和依赖关系。
		ApplicationContext appContext = SpringApplication.run(MovieRecommenderSystemApplication.class, args);

		//使用ApplicationContext来查找正在使用的过滤器
		RecommenderImplementation recommender = appContext.getBean(RecommenderImplementation.class);

		System.out.println(recommender);
		System.out.println(recommender.getFilter());

		//调用方法来获得推荐
		String[] result = recommender.recommendMovies("Finding Dory");


		System.out.println(Arrays.toString(result));
	}
}
