package io.datajek.spring.basics.movierecommendersystem.lesson3;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;

import java.util.Arrays;

@SpringBootApplication
public class MovieRecommenderSystemApplication {


	public static void main(String[] args) {

		
		ApplicationContext appContext=SpringApplication.run(MovieRecommenderSystemApplication.class, args);
		RecommenderImplementation recommender=appContext.getBean(RecommenderImplementation.class);
		//调用方法来获得推荐
		String[] result = recommender.recommendMovies("Finding Dory");

		//显示结果
		System.out.println(Arrays.toString(result));
	}

}
