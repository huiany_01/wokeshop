package io.datajek.spring.basics.movierecommendersystem.lesson9;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ContentBasedFilter implements Filter {
    //用于计数创建的实例
    private static int instances= 0;

    @Autowired
    private Movie movie;

    public ContentBasedFilter(Movie movie){
        super();
        this.movie=movie;
        instances++;
        System.out.println("content-based filter construct called");
    }
    public Movie getMovie() {
        return movie;
    }

    public static int getInstances(){
        return ContentBasedFilter.instances;
    }
    public String[] getRecommendations(String movie){
        //实现基于内容的过滤器的逻辑
        return new String[]{
          "Happy Feet","Ice Age","Shark Tale"
        };
    }
}
