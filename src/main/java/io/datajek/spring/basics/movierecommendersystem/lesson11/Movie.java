package io.datajek.spring.basics.movierecommendersystem.lesson11;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component
@Scope(value=ConfigurableBeanFactory.SCOPE_PROTOTYPE, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class Movie {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private int id;
    private String name;
    private String genre;
    private String producer;

    public Movie() {
        super();
        logger.info("In Movie constructor method");
    }

    @PostConstruct
    private void postConstruct() {
        //初始化代码
        logger.info("In Movie postConstruct method");
    }

    @PreDestroy
    private void preDestroy() {
        //用于清除的代码
        logger.info("In Movie preDestroy method");
    }

    public double movieSimilarity(int movie1, int movie2) {

        double similarity = 0.0;

        //如果genre相同，在相似度上加0.3
        //如果producer相同，在相似度上加0.5

        return similarity;
    }
}