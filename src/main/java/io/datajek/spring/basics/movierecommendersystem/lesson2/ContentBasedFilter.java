package io.datajek.spring.basics.movierecommendersystem.lesson2;

public class ContentBasedFilter implements Filter{

    public String[] getRecommendations(String movie) {

        //实现基于内容过滤器逻辑

        //返回电影推荐
        return new String[] {"Happy Feet", "Ice Age", "Shark Tale"};
    }

}