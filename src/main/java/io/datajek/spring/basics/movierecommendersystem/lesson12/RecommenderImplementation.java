package io.datajek.spring.basics.movierecommendersystem.lesson12;

import javax.inject.Inject;
import javax.inject.Named;
import org.springframework.beans.factory.annotation.Qualifier;

@Named
public class RecommenderImplementation {

    @Inject
    @Qualifier("CF")
    private Filter filter;

    public Filter getFilter() {
        return filter;
    }

    //使用过滤器来获得推荐
    public String [] recommendMovies (String movie) {

        //打印正在使用的接口实现的名称
        System.out.println("\nName of the filter in use: " + filter + "\n");

        String[] results = filter.getRecommendations("Finding Dory");

        return results;
    }
}
