package io.datajek.spring.basics.movierecommendersystem.lesson2;

import java.util.Arrays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovieRecommenderSystemApplication {

	public static void main(String[] args) {

		//SpringApplication.run(MovieRecommenderSystemApplication.class, args);

		//将过滤器的名称作为构造函数的参数传递
		RecommenderImplementation recommender = new RecommenderImplementation(new ContentBasedFilter());

		//调用方法获得推荐
		String[] result = recommender.recommendMovies("Finding Dory");

		//显示结果
		System.out.println(Arrays.toString(result));

	}

}

