package io.datajek.spring.basics.movierecommendersystem.lesson5;

import org.springframework.stereotype.Component;

@Component
public class ContentBasedFilter implements Filter {
    public String[] getRecommendations(String movie){
        //实现基于内容的过滤器的逻辑
        return new String[]{
          "Happy Feet","Ice Age","Shark Tale"
        };
    }
}
