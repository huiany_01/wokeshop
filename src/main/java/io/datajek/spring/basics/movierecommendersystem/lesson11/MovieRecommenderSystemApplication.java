package io.datajek.spring.basics.movierecommendersystem.lesson11;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class MovieRecommenderSystemApplication {


	public static void main(String[] args) {

		
		ApplicationContext appContext=SpringApplication.run(MovieRecommenderSystemApplication.class, args);
		System.out.println();
		RecommenderImplementation recommender=appContext.getBean(RecommenderImplementation.class);
		System.out.println(recommender);

		System.out.println();
		Movie m1 = appContext.getBean(Movie.class);
		System.out.println(m1);

		System.out.println();
		Movie m2 = appContext.getBean(Movie.class);
		System.out.println(m2);

		System.out.println();

	}

}
