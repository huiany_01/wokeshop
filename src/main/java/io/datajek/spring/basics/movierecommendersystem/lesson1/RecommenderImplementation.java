package io.datajek.spring.basics.movierecommendersystem.lesson1;

public class RecommenderImplementation {

    public String [] recommendMovies (String movie) {

        //使用基于内容的过滤器来寻找类似的电影

        ContentBasedFilter filter = new ContentBasedFilter();
        String[] results = filter.getRecommendations("Finding Dory");

        //返回结果
        //return new String[] {"M1", "M2", "M3"};
        return results;
    }

}