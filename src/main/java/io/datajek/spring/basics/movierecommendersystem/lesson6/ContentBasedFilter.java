package io.datajek.spring.basics.movierecommendersystem.lesson6;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("CBF")
public class ContentBasedFilter implements Filter {
    public String[] getRecommendations(String movie){
        //实现基于内容的过滤器的逻辑
        return new String[]{
          "Happy Feet","Ice Age","Shark Tale"
        };
    }
}
