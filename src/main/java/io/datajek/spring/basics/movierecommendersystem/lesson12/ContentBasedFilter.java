package io.datajek.spring.basics.movierecommendersystem.lesson12;

import javax.inject.Named;
import org.springframework.beans.factory.annotation.Qualifier;

@Named
@Qualifier("CBF")
public class ContentBasedFilter implements Filter{

    //getRecommendations将一部电影作为输入，并返回类似电影的列表。
    public String[] getRecommendations(String movie) {
        //实现基于内容的过滤器的逻辑
        return new String[] {"Happy Feet", "Ice Age", "Shark Tale"};
    }
}