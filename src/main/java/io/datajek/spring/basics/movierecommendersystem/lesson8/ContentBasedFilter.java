package io.datajek.spring.basics.movierecommendersystem.lesson8;

import org.springframework.stereotype.Component;

@Component
public class ContentBasedFilter implements Filter {

    public ContentBasedFilter(){
        super();
        System.out.println("content-based filter construct called");
    }
    public String[] getRecommendations(String movie){
        //实现基于内容的过滤器的逻辑
        return new String[]{
          "Happy Feet","Ice Age","Shark Tale"
        };
    }
}
