package io.datajek.spring.basics.movierecommendersystem.lesson2;

public class RecommenderImplementation {

    //使用过滤器接口选择过滤器
    private Filter filter;

    public RecommenderImplementation(Filter filter) {
        super();
        this.filter = filter;
    }

    //使用过滤器来获得推荐
    public String [] recommendMovies (String movie) {

        //打印正在使用的接口实现的名称
        System.out.println("正在使用的过滤器的名称：" + filter + "\n");

        String[] results = filter.getRecommendations("Finding Dory");

        return results;
    }

}