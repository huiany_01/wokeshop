package io.datajek.spring.basics.movierecommendersystem.lesson1;

import java.util.Arrays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class MovieRecommenderSystemApplication {

	public static void main(String[] args) {

		//SpringApplication.run(MovieRecommenderSystemApplication.class, args);

		//创建 RecommenderImplementation 类的对象
		RecommenderImplementation recommender = new RecommenderImplementation();

		//调用方法来获得推荐
		String[] result = recommender.recommendMovies("Finding Dory");

		//显示结果
		System.out.println(Arrays.toString(result));

	}

}
